#!/usr/bin/env python
# coding: utf-8

# In[1]:


import json
import request
import flask
#from flask import json
import pickle
from flask import request
app = flask.Flask(__name__)
app.config["DEBUG"] = True
import docx2txt
import spacy


# In[3]:


nep = spacy.load('baluwa_model')


# In[ ]:





# In[4]:


@app.route('/',methods=['GET','POST'])
def load_trained_model():
    file = request.files['docfile']
    my = docx2txt.process(file)
    doc_to_test=nep(my)
    f=[]
    g=[]
    for ent in doc_to_test.ents:
        f.append(ent.text)
        g.append(ent.label_)
    o=[]
    for i,j in zip(f,g):
        if j=="exp":
            o.append(i)
    p=[]
    for i,j in zip(f,g):
        if j=="company":
            p.append(i)
    q=[]
    for i,j in zip(f,g):
        if j=="edate":
            q.append(i)
    person_info = []
    for i,j in zip(f,g):
    	if j=="person":
    		person_info.append(i)
    	if j=="email":
    		person_info.append(i)

    	if j=="number":
    		person_info.append(i)
    pos_of_date=[]
    for i in q:
        pos_of_date.append(my.index(i))
    
    string_of_date =[]
    for i in pos_of_date:
        string_of_date.append(my[i-200:i+200])
    # print(string_of_date)
    import re
    des=[]
    for i in string_of_date:
        count = 0
        for j in o:
            if re.search(j,i):
                count+=1
                des.append(j)
#             print(j)
                break
        if (count == 0):
            des.append("null")
    # import re
    com=[]
    for i in string_of_date:
        count = 0
#     for j in o:
#         if re.search(j,i):
#             count+=1
#             print(j)
#             break
        for k in p:
            if re.search(k,i):
                count+=1
                com.append(k)
                break
        if (count == 0):
            com.append("null")
    dates_des=[]
    date_des =dict(zip(q,des))
    for k,v in date_des.items():
    	l=k+"=========="+v
    	dates_des.append(l)
    # print(dates_des)
    dict_of_com_date_des=dict(zip(dates_des,com))
    combine_dic=[]
    for k1,v1 in dict_of_com_date_des.items():
    	m=k1+"========="+v1
    	combine_dic.append(m)
    education=[]
    uni=[]
    cdate=[]
    for i,j in zip(f,g):
        if j=="education":
            education.append(i)
        
    for i,j in zip(f,g):
        if j=="cdate":
            cdate.append(i)
    for i,j in zip(f,g):
        if j=="uni":
            uni.append(i)
    somedict = { 
                 "designation_details":combine_dic,
                 "personel_information":person_info,
                 "education":education,
                 "universities":uni,
                 "education_date":cdate,
                 "company_only":p,
                 "exp_only":o
        }
    test_predicted = json.dumps(somedict)
    return test_predicted
    
# In[ ]:


app.run(host="192.168.0.149",port=5000)

